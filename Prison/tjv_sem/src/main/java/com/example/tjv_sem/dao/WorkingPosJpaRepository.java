package com.example.tjv_sem.dao;

import com.example.tjv_sem.domain.WorkingPosition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkingPosJpaRepository extends JpaRepository<WorkingPosition, String> {
}
