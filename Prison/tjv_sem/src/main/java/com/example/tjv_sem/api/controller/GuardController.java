package com.example.tjv_sem.api.controller;

import com.example.tjv_sem.api.controller.dto.GuardDto;
import com.example.tjv_sem.api.controller.dto.PrisonerDto;
import com.example.tjv_sem.api.controller.dto.converter.GuardConverter;
import com.example.tjv_sem.api.controller.dto.converter.PrisonerConverter;
import com.example.tjv_sem.business.guard.SecurityGuardService;
import com.example.tjv_sem.domain.SecurityGuard;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/guards")
public class GuardController implements ControllerCRUD<Long, SecurityGuard, GuardDto>{

    private final SecurityGuardService securityGuardService;
    private final GuardConverter guardConverter;
    private final PrisonerConverter prisonerConverter;

    public GuardController(SecurityGuardService securityGuardService, GuardConverter guardConverter, PrisonerConverter prisonerConverter) {
        this.securityGuardService = securityGuardService;
        this.guardConverter = guardConverter;
        this.prisonerConverter = prisonerConverter;
    }

    @Override
    @GetMapping("/{id}")
    public GuardDto getById(@PathVariable Long id) {
        return guardConverter.toDto(securityGuardService.readByID(id));
    }

    @Override
    @PostMapping
    public GuardDto create(@RequestBody GuardDto guardDto) {
        return guardConverter.toDto(securityGuardService.create(guardConverter.toEntity(guardDto)));
    }

    @Override
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        securityGuardService.deleteById(id);
    }

    @Override
    @PutMapping("/{id}")
    public GuardDto update(@PathVariable Long id,@RequestBody GuardDto guardDto) {
        return guardConverter.toDto(securityGuardService.update(id, guardConverter.toEntity(guardDto)));
    }

    @GetMapping("/{id}/prisoners")
    public List<PrisonerDto> readAllObserved(@PathVariable Long id){
        return prisonerConverter.toDtoCol(securityGuardService.readObserved(id));
    }

    @PostMapping("/{idGuard}/prisoners/{idPrisoner}")
    public GuardDto addObserved(@PathVariable Long idGuard, @PathVariable Long idPrisoner){
        return guardConverter.toDto(securityGuardService.addObserve(idGuard, idPrisoner));
    }

    @DeleteMapping("/{idGuard}/prisoners/{idPrisoner}")
    public GuardDto deleteObserved(@PathVariable Long idGuard, @PathVariable Long idPrisoner){
        return guardConverter.toDto(securityGuardService.deleteObserve(idGuard, idPrisoner));
    }

    @PostMapping("/{idGuard}/positions/{idWorkPos}")
    public GuardDto addPositionToGuard(@PathVariable Long idGuard, @PathVariable String idWorkPos){
        return guardConverter.toDto(securityGuardService.changeWorkPos(idGuard, idWorkPos));
    }

}
