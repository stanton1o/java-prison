package com.example.tjv_sem.business.prisoner;

import com.example.tjv_sem.domain.Prisoner;
import com.example.tjv_sem.domain.SecurityGuard;

import java.util.List;

public interface PrisonerService {

    Prisoner create(Prisoner prisoner);
    Prisoner update(Long id, Prisoner prisoner);
    Prisoner readByID(Long id);
    void deleteById(Long id);
    List<SecurityGuard> readAllWatched(Long id);
    Prisoner addPrisonerToCell(Long idPrisoner, Long idCell);
}
