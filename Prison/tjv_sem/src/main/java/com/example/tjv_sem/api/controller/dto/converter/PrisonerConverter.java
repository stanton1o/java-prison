package com.example.tjv_sem.api.controller.dto.converter;

import com.example.tjv_sem.api.controller.dto.PrisonerDto;
import com.example.tjv_sem.api.exception.EntityStateException;
import com.example.tjv_sem.business.cell.CellService;
import com.example.tjv_sem.domain.Prisoner;
import com.example.tjv_sem.domain.SecurityGuard;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public record PrisonerConverter(CellService cellService) {

    public PrisonerDto toDto(Prisoner prisoner) {
        var dto = new PrisonerDto();
        dto.setIdPrisoner(prisoner.getIdPrisoner());
        dto.setName(prisoner.getName());
        dto.setRegime(prisoner.getRegime());
        dto.setIdCell(prisoner.getCell().getIdCell());
        dto.setIdWatched(prisoner.getWatched().stream().map(SecurityGuard::getIdGuard).collect(Collectors.toList()));
        return dto;
    }

    public Prisoner toEntity(PrisonerDto prisonerDto) {
        if(prisonerDto.getName() == null || prisonerDto.getIdCell() == null || prisonerDto.getRegime() == null)
            throw new EntityStateException("Not null argument is null");
        return new Prisoner(prisonerDto.getIdPrisoner(), prisonerDto.getName(), prisonerDto.getRegime(), cellService.readByID(prisonerDto.getIdCell()));
    }

    public List<PrisonerDto> toDtoCol(Collection<Prisoner> prisoners) {
        return prisoners.stream().map(this::toDto).toList();
    }

    public List<Prisoner> toEntityCol(Collection<PrisonerDto> prisonersDto) {
        return prisonersDto.stream().map(this::toEntity).toList();
    }
}
