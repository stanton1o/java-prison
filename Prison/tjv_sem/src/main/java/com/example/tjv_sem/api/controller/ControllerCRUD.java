package com.example.tjv_sem.api.controller;

public interface ControllerCRUD<I, E, D> {

    D getById(I id);

    D create(D d);

    void delete(I id);

    D update(I id, D d);
}
