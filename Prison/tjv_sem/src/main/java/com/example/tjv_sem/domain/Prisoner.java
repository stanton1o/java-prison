package com.example.tjv_sem.domain;

import javax.persistence.*;
import java.util.*;

@Entity
public class Prisoner {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPrisoner;
    private String name;
    private String regime;

    @ManyToOne
    @JoinColumn(name = "cell_id")
    private Cell cell;

    @ManyToMany(mappedBy = "observed", fetch = FetchType.EAGER) // загуглить потом)
    private List<SecurityGuard> watched = new ArrayList<>();

    public Prisoner(Long idPrisoner, String name, String regime, Cell cell) {
        this.idPrisoner = idPrisoner;
        this.name = Objects.requireNonNull(name);
        this.regime = Objects.requireNonNull(regime);
        this.cell = Objects.requireNonNull(cell);
    }

    public Prisoner() {}

    public Cell getCell() {
        return cell;
    }

    public void setCell(Cell cell) {
        this.cell = cell;
    }

    public Long getIdPrisoner() {
        return idPrisoner;
    }

    public void setIdPrisoner(Long idPrisoner) {
        this.idPrisoner = idPrisoner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegime() {
        return regime;
    }

    public void setRegime(String regime) {
        this.regime = regime;
    }


    public List<SecurityGuard> getWatched() {
        return watched;
    }

    public void setWatched(List<SecurityGuard> watched) {
        this.watched = watched;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Prisoner prisoner = (Prisoner) o;
        return idPrisoner.equals(prisoner.idPrisoner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPrisoner);
    }
}
