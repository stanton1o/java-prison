package com.example.tjv_sem.api.controller.dto.converter;

import com.example.tjv_sem.api.controller.dto.WorkPosDto;
import com.example.tjv_sem.api.exception.EntityStateException;
import com.example.tjv_sem.business.guard.SecurityGuardService;
import com.example.tjv_sem.domain.SecurityGuard;
import com.example.tjv_sem.domain.WorkingPosition;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public record WorkPosConverter(SecurityGuardService securityGuardService) {
    public WorkPosDto toDto(WorkingPosition workingPosition) {
        var dto = new WorkPosDto();
        dto.setMinSalary(workingPosition.getMinSalary());
        dto.setPositionName(workingPosition.getPositionName());
        dto.setIdSecurityGuards(workingPosition.getSecurityGuards().stream().map(SecurityGuard::getIdGuard).toList());
        return dto;
    }

    public WorkingPosition toEntity(WorkPosDto workPosDto) {
        if(workPosDto.getMinSalary() == null || workPosDto.getPositionName() == null)
            throw new EntityStateException("Not null argument is null");
        if(workPosDto.getIdSecurityGuards() == null)
            return new WorkingPosition(workPosDto.getPositionName(), workPosDto.getMinSalary(), new ArrayList<>());
        return new WorkingPosition(workPosDto.getPositionName(), workPosDto.getMinSalary(),
                workPosDto.getIdSecurityGuards().stream().map(securityGuardService::readByID).toList());
    }

    public List<WorkPosDto> toDtoCol(Collection<WorkingPosition> workingPositions){
        return workingPositions.stream().map(this::toDto).toList();
    }

    public List<WorkingPosition> toEntityCol(Collection<WorkPosDto> workPossDto){
        return workPossDto.stream().map(this::toEntity).toList();
    }
}
