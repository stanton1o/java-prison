package com.example.tjv_sem.dao;

import com.example.tjv_sem.domain.SecurityGuard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SecurityGuardJpaRepository extends JpaRepository<SecurityGuard, Long>{
}
