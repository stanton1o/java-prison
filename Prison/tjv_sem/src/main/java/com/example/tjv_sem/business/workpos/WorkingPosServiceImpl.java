package com.example.tjv_sem.business.workpos;

import com.example.tjv_sem.api.exception.EntityNotFoundException;
import com.example.tjv_sem.api.exception.EntityStateException;
import com.example.tjv_sem.dao.SecurityGuardJpaRepository;
import com.example.tjv_sem.dao.WorkingPosJpaRepository;
import com.example.tjv_sem.domain.SecurityGuard;
import com.example.tjv_sem.domain.WorkingPosition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
@Component
public class WorkingPosServiceImpl implements WorkingPosService {

    private WorkingPosJpaRepository workingPosJpaRepository;
    private SecurityGuardJpaRepository guardJpaRepository;

    @Autowired
    public WorkingPosServiceImpl(WorkingPosJpaRepository workingPosJpaRepository,
                                 SecurityGuardJpaRepository guardJpaRepository) {
        this.workingPosJpaRepository = workingPosJpaRepository;
        this.guardJpaRepository = guardJpaRepository;
    }

    public WorkingPosServiceImpl() {
    }

    @Override
    public WorkingPosition create(WorkingPosition workingPosition) {
        if (workingPosition.getPositionName() == null)
            throw new EntityStateException("Working position must has a name position");
        if (workingPosJpaRepository.existsById(workingPosition.getPositionName()))
            throw new EntityStateException
                    ("Working position " + workingPosition.getPositionName() + " already exists.");
        return workingPosJpaRepository.save(workingPosition);
    }

    @Override
    public WorkingPosition update(String id, WorkingPosition workingPosition) {
        WorkingPosition workPosToUpd = readByID(id);

        for (var guard: workPosToUpd.getSecurityGuards()) {
            if(guard.getSalary() < workingPosition.getMinSalary()){
                guard.setSalary(workingPosition.getMinSalary());
                guardJpaRepository.save(guard);
            }
        }
        workPosToUpd.setMinSalary(workingPosition.getMinSalary());
        return workingPosJpaRepository.save(workPosToUpd);
    }

    @Override
    public WorkingPosition readByID(String id) {
        return workingPosJpaRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Working position " + id + " not found."));
    }

    @Override
    public void deleteById(String id) {
        WorkingPosition pos = readByID(id);
        if (!pos.getSecurityGuards().isEmpty())
            throw new EntityStateException("A guard is working at the " + pos.getPositionName() + " , cannot be removed");
        workingPosJpaRepository.deleteById(pos.getPositionName());
    }

    @Override
    public List<SecurityGuard> readAllGuards(String id) {
        return readByID(id).getSecurityGuards();
    }
}
