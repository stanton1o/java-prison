package com.example.tjv_sem.api.controller.dto;

import java.util.ArrayList;
import java.util.List;

public class PrisonerDto {
    private Long idPrisoner;
    private String name;
    private String regime;
    private Long idCell;
    private List<Long> idWatched = new ArrayList<>();

    public Long getIdPrisoner() {
        return idPrisoner;
    }

    public void setIdPrisoner(Long idPrisoner) {
        this.idPrisoner = idPrisoner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegime() {
        return regime;
    }

    public void setRegime(String regime) {
        this.regime = regime;
    }

    public Long getIdCell() {
        return idCell;
    }

    public void setIdCell(Long idCell) {
        this.idCell = idCell;
    }

    public List<Long> getIdWatched() {
        return idWatched;
    }

    public void setIdWatched(List<Long> idWatched) {
        this.idWatched = idWatched;
    }
}
