package com.example.tjv_sem.dao;

import com.example.tjv_sem.domain.Cell;
import com.example.tjv_sem.domain.Prisoner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PrisonerJpaRepository extends JpaRepository<Prisoner, Long> {
}
