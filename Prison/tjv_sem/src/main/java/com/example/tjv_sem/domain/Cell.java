package com.example.tjv_sem.domain;

import javax.persistence.*;
import java.util.*;

@Entity
public class Cell {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idCell;
    private Double size;
    private Double comfort;

    @OneToMany(mappedBy = "cell")
    private List<Prisoner> prisoners = new ArrayList<>();

    public Cell(Long idCell, Double size, Double comfort) {
        this.idCell = idCell;
        this.size = Objects.requireNonNull(size);
        this.comfort = Objects.requireNonNull(comfort);
    }

    public Cell() {
    }

    public Long getIdCell() {
        return idCell;
    }

    public List<Prisoner> getPrisoners() {
        return prisoners;
    }

    public void setPrisoners(List<Prisoner> prisoners) {
        this.prisoners = prisoners;
    }

    public void setIdCell(Long idCell) {
        this.idCell = idCell;
    }

    public Double getSize() {
        return size;
    }

    public void setSize(Double size) {
        this.size = size;
    }

    public Double getComfort() {
        return comfort;
    }

    public void setComfort(Double comfort) {
        this.comfort = comfort;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell ceil = (Cell) o;
        return idCell.equals(ceil.idCell);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCell);
    }
}
