package com.example.tjv_sem.business.prisoner;

import com.example.tjv_sem.api.exception.EntityNotFoundException;
import com.example.tjv_sem.api.exception.EntityStateException;
import com.example.tjv_sem.business.cell.CellService;
import com.example.tjv_sem.dao.PrisonerJpaRepository;
import com.example.tjv_sem.dao.SecurityGuardJpaRepository;
import com.example.tjv_sem.domain.Cell;
import com.example.tjv_sem.domain.Prisoner;
import com.example.tjv_sem.domain.SecurityGuard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class PrisonerServiceImpl implements PrisonerService{

    private PrisonerJpaRepository prisonerJpaRepository;
    private SecurityGuardJpaRepository securityGuardJpaRepository;
    private CellService cellService;

    @Autowired
    public PrisonerServiceImpl(PrisonerJpaRepository prisonerJpaRepository,
                               SecurityGuardJpaRepository securityGuardJpaRepository, CellService cellService) {
        this.prisonerJpaRepository = prisonerJpaRepository;
        this.securityGuardJpaRepository = securityGuardJpaRepository;
        this.cellService = cellService;
    }

    public PrisonerServiceImpl() {}

    @Override
    public Prisoner create(Prisoner prisoner) {
        int MAX_PRISONERS = 6;
        long RATIO = 10;

        if(prisoner.getIdPrisoner() != null && prisonerJpaRepository.existsById(prisoner.getIdPrisoner()))
            throw new EntityStateException("Prisoner with id " + prisoner.getIdPrisoner() + " already exists.");

        if (!(prisoner.getCell().getPrisoners().isEmpty()
                || prisoner.getCell().getPrisoners().iterator().next().getRegime().equals(prisoner.getRegime())))
            throw new EntityStateException("All prisoners in the cell must have the same regime");

        if(prisoner.getCell().getPrisoners().size() >= MAX_PRISONERS)
            throw new EntityStateException("There are no empty seats in the cell");

        if(securityGuardJpaRepository.count() * RATIO <= prisonerJpaRepository.count())
            throw new EntityStateException("Not enough security guards");

        return prisonerJpaRepository.save(prisoner);
    }

    @Override
    public Prisoner update(Long id, Prisoner prisoner) {
        Prisoner prisonerToUpdate = readByID(id);
        prisonerToUpdate.setName(prisoner.getName());
        for (var pris: prisonerToUpdate.getCell().getPrisoners()) {
            pris.setRegime(prisoner.getRegime());
            prisonerJpaRepository.save(pris);
        }
        return prisonerJpaRepository.save(prisonerToUpdate);
    }

    @Override
    public Prisoner readByID(Long id) {
        return prisonerJpaRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Prisoner with id " + id + " not found."));
    }

    @Override
    public void deleteById(Long id) {
       prisonerJpaRepository.deleteById(readByID(id).getIdPrisoner());
    }

    @Override
    public List<SecurityGuard> readAllWatched(Long id) {
        return readByID(id).getWatched();
    }

    @Override
    public Prisoner addPrisonerToCell(Long idPrisoner, Long idCell) {
        Prisoner prisoner = readByID(idPrisoner);

        int MAX_PRISONERS = 6;
        Cell cell = cellService.readByID(idCell);
        if (cell.getPrisoners().size() >= MAX_PRISONERS)
            throw new EntityStateException("There are no empty seats in the cell");
        if (!(cell.getPrisoners().isEmpty()
                || cell.getPrisoners().iterator().next().getRegime().equals(prisoner.getRegime())))
            throw new EntityStateException("All prisoners in the cell must have the same regime");

        prisoner.setCell(cell);

        return prisonerJpaRepository.save(prisoner);
    }
}
