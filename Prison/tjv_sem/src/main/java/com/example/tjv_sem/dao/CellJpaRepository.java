package com.example.tjv_sem.dao;

import com.example.tjv_sem.domain.Cell;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CellJpaRepository extends JpaRepository<Cell, Long> {
    @Query(value = "select cell.* from cell join\n" +
            "(select * from prisoner join\n" +
            "    (select prisoner_id from security_prisoner join\n" +
            "        (select id_guard from security_guard where salary >= :salary)r0\n" +
            "        on r0.id_guard = security_prisoner.security_id)r1\n" +
            "    on r1.prisoner_id = prisoner.id_prisoner)r2\n" +
            "on cell.id_cell = r2.cell_id", nativeQuery = true)

    List<Cell> findCellsBySalary(@Param("salary") Long salary);
}
