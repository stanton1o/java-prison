package com.example.tjv_sem.api.exception;

public class EntityStateException extends RuntimeException {
    public EntityStateException() {
        super("Illegal state of entity");
    }

    public EntityStateException(String message) {
        super(message);
    }

    public <E> EntityStateException(E entity) {
        super("Illegal state of entity " + entity);
    }

}
