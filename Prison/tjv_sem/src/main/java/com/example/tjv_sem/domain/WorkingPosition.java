package com.example.tjv_sem.domain;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class WorkingPosition {

    @Id
    private String positionName;
    private Long minSalary;

    @OneToMany(mappedBy = "workingPosition")
    private List<SecurityGuard> securityGuards;

    public WorkingPosition(String positionName, Long minSalary, List<SecurityGuard> securityGuards) {
        this.positionName = Objects.requireNonNull(positionName);
        this.minSalary = Objects.requireNonNull(minSalary);
        this.securityGuards = securityGuards;
    }

    public WorkingPosition() {
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public Long getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(Long minSalary) {
        this.minSalary = minSalary;
    }

    public List<SecurityGuard> getSecurityGuards() {
        return securityGuards;
    }

    public void setSecurityGuards(List<SecurityGuard> securityGuards) {
        this.securityGuards = securityGuards;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WorkingPosition that = (WorkingPosition) o;
        return positionName.equals(that.positionName) && Objects.equals(securityGuards, that.securityGuards);
    }

    @Override
    public int hashCode() {
        return Objects.hash(positionName, securityGuards);
    }
}
