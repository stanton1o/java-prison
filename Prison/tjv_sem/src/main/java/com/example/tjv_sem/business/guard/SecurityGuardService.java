package com.example.tjv_sem.business.guard;

import com.example.tjv_sem.domain.Prisoner;
import com.example.tjv_sem.domain.SecurityGuard;

import java.util.List;

public interface SecurityGuardService {
     SecurityGuard create(SecurityGuard securityGuard);
     SecurityGuard update(Long id, SecurityGuard securityGuard);
     SecurityGuard readByID(Long id);
     void deleteById(Long id);
     SecurityGuard addObserve(Long idSecurityGuard, Long idPrisoner);
     SecurityGuard deleteObserve(Long idSecurityGuard, Long idPrisoner);
     List<Prisoner> readObserved(Long id);
     SecurityGuard changeWorkPos(Long id, String pos);
}
