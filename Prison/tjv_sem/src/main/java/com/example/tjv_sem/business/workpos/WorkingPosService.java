package com.example.tjv_sem.business.workpos;

import com.example.tjv_sem.domain.SecurityGuard;
import com.example.tjv_sem.domain.WorkingPosition;

import java.util.List;

public interface WorkingPosService {
    WorkingPosition create(WorkingPosition workingPosition);
    WorkingPosition update(String id, WorkingPosition workingPosition);
    WorkingPosition readByID(String id);
    void deleteById(String id);
    List<SecurityGuard> readAllGuards(String id);
}
