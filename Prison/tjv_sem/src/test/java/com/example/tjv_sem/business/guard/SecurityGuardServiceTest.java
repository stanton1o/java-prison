package com.example.tjv_sem.business.guard;

import com.example.tjv_sem.api.exception.EntityNotFoundException;
import com.example.tjv_sem.api.exception.EntityStateException;
import com.example.tjv_sem.dao.PrisonerJpaRepository;
import com.example.tjv_sem.dao.SecurityGuardJpaRepository;
import com.example.tjv_sem.dao.WorkingPosJpaRepository;
import com.example.tjv_sem.domain.Cell;
import com.example.tjv_sem.domain.Prisoner;
import com.example.tjv_sem.domain.SecurityGuard;
import com.example.tjv_sem.domain.WorkingPosition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class SecurityGuardServiceTest {

    @Autowired
    private SecurityGuardService guardService;
    @MockBean
    private SecurityGuardJpaRepository SGRep;
    @MockBean
    private PrisonerJpaRepository prisRep;
    @MockBean
    private WorkingPosJpaRepository WPRep;

    private final WorkingPosition wp = new WorkingPosition("Test", 5000L, new ArrayList<>());
    private final Prisoner prisoner = new Prisoner(1L, "Bob", "Norm", new Cell());
    private final SecurityGuard guardFound = new SecurityGuard(1L, "Anton", wp, 5000L);
    private final SecurityGuard guardFoundErr = new SecurityGuard(88L, "Anton", wp, 500L);
    private final SecurityGuard guardToDel= new SecurityGuard(90L, "Anton", wp, 5000L);
    private final SecurityGuard guardUpd= new SecurityGuard(1L, "Antonio", wp, 15000L);
    private final SecurityGuard guardExists = new SecurityGuard(2L, "Antonio", wp, 5000L);
    private final SecurityGuard guardNotExists = new SecurityGuard(404L, "Feramir", wp, 5000L);

    @BeforeEach
    void setUp() {
        Mockito.when(WPRep.findById(wp.getPositionName())).thenReturn(Optional.of(wp));
        Mockito.when(prisRep.findById(prisoner.getIdPrisoner())).thenReturn(Optional.of(prisoner));
        Mockito.when(SGRep.findById(guardFound.getIdGuard())).thenReturn(Optional.of(guardFound));
        Mockito.when(SGRep.findById(guardToDel.getIdGuard())).thenReturn(Optional.of(guardToDel));
        Mockito.when(SGRep.findById(guardFoundErr.getIdGuard())).thenThrow(EntityStateException.class);
        Mockito.when(SGRep.existsById(guardExists.getIdGuard())).thenThrow(EntityStateException.class);
        Mockito.when(SGRep.findById(guardNotExists.getIdGuard())).thenThrow(EntityNotFoundException.class);
        Mockito.when(SGRep.existsById(guardNotExists.getIdGuard())).thenThrow(EntityNotFoundException.class);
    }

    @Test
    void create() {
        guardService.create(guardFound);
        Assertions.assertEquals(Optional.of(guardFound), SGRep.findById(guardFound.getIdGuard()));
        Mockito.verify(SGRep, Mockito.times(1)).save(guardFound);
        Assertions.assertThrows(EntityStateException.class, ()-> guardService.create(guardFoundErr));
    }
    @Test
    void createExists() {
        Assertions.assertThrows(EntityStateException.class, () -> guardService.create(guardExists));
        Mockito.verify(SGRep, Mockito.times(0)).save(guardExists);
    }

    @Test
    void update() {
        assertEquals(guardFound.getIdGuard(), guardUpd.getIdGuard());
        assertNotEquals(guardFound.getSalary(), guardUpd.getSalary());
        assertNotEquals(guardFound.getName(), guardUpd.getName());
        guardService.update(guardFound.getIdGuard(), guardUpd);
        Mockito.verify(SGRep, Mockito.times(1)).save(guardUpd);
        guardFoundErr.setIdGuard(guardFound.getIdGuard());
        Assertions.assertThrows(EntityStateException.class,
                ()-> guardService.update(guardFoundErr.getIdGuard(), guardFoundErr));
    }

    @Test
    void updateNotExists() {
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> guardService.update(guardNotExists.getIdGuard(), guardNotExists));
        Mockito.verify(SGRep, Mockito.times(0)).save(guardNotExists);
    }

    @Test
    void readByID() {
        Assertions.assertEquals(guardService.readByID(guardFound.getIdGuard()), guardFound);
        Assertions.assertThrows(EntityNotFoundException.class,() -> guardService.readByID(guardNotExists.getIdGuard()));
    }

    @Test
    void deleteById() {
        Mockito.when(SGRep.count()).thenReturn(Long.valueOf(1));
        Mockito.when(prisRep.count()).thenReturn(Long.valueOf(0));

        guardService.create(guardFound);
        guardService.deleteById(guardFound.getIdGuard());
        Mockito.verify(SGRep, Mockito.times(1)).deleteById(guardFound.getIdGuard());
    }

    @Test
    void deleteByIdNotExists() {
        Mockito.when(SGRep.count()).thenReturn(Long.valueOf(1));
        Mockito.when(prisRep.count()).thenReturn(Long.valueOf(0));
        Assertions.assertThrows(EntityNotFoundException.class, ()-> guardService.deleteById(guardNotExists.getIdGuard()));
        Mockito.verify(SGRep, Mockito.times(0)).deleteById(guardNotExists.getIdGuard());
    }

    @Test
    void addObserve() {
        var g = guardFound;
        g.getObserved().add(prisoner);
        Mockito.when(SGRep.save(guardFound)).thenReturn(g);
        Assertions.assertEquals(List.of(prisoner),
                guardService.addObserve(guardFound.getIdGuard(), prisoner.getIdPrisoner()).getObserved());
    }

    @Test
    void addObserveNotExists() {
        Assertions.assertThrows(EntityNotFoundException.class,
                ()-> guardService.addObserve(guardNotExists.getIdGuard(), prisoner.getIdPrisoner()));
        Mockito.verify(SGRep, Mockito.times(0)).save(guardNotExists);
    }

    @Test
    void deleteObserve() {
        var g = guardFound;
        guardFound.getObserved().add(prisoner);
        Mockito.when(SGRep.save(guardFound)).thenReturn(g);
        Assertions.assertEquals(new ArrayList<>(),
                guardService.deleteObserve(guardFound.getIdGuard(), prisoner.getIdPrisoner()).getObserved());
    }

    @Test
    void deleteObserveNotExists() {
        Assertions.assertThrows(EntityNotFoundException.class,
                ()-> guardService.deleteObserve(guardNotExists.getIdGuard(), prisoner.getIdPrisoner()));
        Mockito.verify(SGRep, Mockito.times(0)).save(guardNotExists);
    }

    @Test
    void readObserved() {
        Prisoner p1 = new Prisoner(1L, "Anton", "Normal", new Cell());
        Prisoner p2 = new Prisoner(2L, "Antonio", "Normal", new Cell());
        var prisoners = List.of(p1,p2,prisoner);
        guardFound.setObserved(prisoners);
        Assertions.assertEquals(guardService.readObserved(guardFound.getIdGuard()), prisoners);
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> guardService.readObserved(guardNotExists.getIdGuard()));
    }

    @Test
    void changeWorkPos() {
        var newWp = new WorkingPosition("NewTest", 50000L, new ArrayList<>());
        Mockito.when(WPRep.findById(newWp.getPositionName())).thenReturn(Optional.of(newWp));
        var g = guardFound;
        g.setWorkingPosition(newWp);
        g.setSalary(newWp.getMinSalary());
        Mockito.when(SGRep.save(guardFound)).thenReturn(g);
        var result = guardService.changeWorkPos(guardFound.getIdGuard(), newWp.getPositionName());
        Assertions.assertEquals(g.getWorkingPosition(), result.getWorkingPosition());
        Assertions.assertEquals(g.getSalary(), result.getSalary());
    }
}