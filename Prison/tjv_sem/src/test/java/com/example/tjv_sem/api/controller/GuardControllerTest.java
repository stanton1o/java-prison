package com.example.tjv_sem.api.controller;

import com.example.tjv_sem.api.controller.dto.converter.GuardConverter;
import com.example.tjv_sem.api.controller.dto.converter.PrisonerConverter;
import com.example.tjv_sem.api.exception.EntityNotFoundException;
import com.example.tjv_sem.api.exception.EntityStateException;
import com.example.tjv_sem.business.cell.CellService;
import com.example.tjv_sem.business.guard.SecurityGuardService;
import com.example.tjv_sem.business.workpos.WorkingPosService;
import com.example.tjv_sem.domain.Cell;
import com.example.tjv_sem.domain.Prisoner;
import com.example.tjv_sem.domain.SecurityGuard;
import com.example.tjv_sem.domain.WorkingPosition;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(GuardController.class)
class GuardControllerTest {


    @MockBean
    SecurityGuardService guardService;
    @MockBean
    CellService cellService;
    @MockBean
    WorkingPosService wpService;
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    MockMvc mockMvc;

    @Autowired
    GuardConverter guardConverter;
    @Autowired
    PrisonerConverter prisonerConverter;

    @TestConfiguration
    static class TestConfig {

        @Bean
        public GuardConverter guardConverter(WorkingPosService wp) {
            return new GuardConverter(wp);
        }

        @Bean
        public PrisonerConverter prisonerConverter(CellService cellService){
            return new PrisonerConverter(cellService);
        }

    }
    @Test
    void testGetById() throws Exception {
        WorkingPosition wp = new WorkingPosition("Normal", 1000L, new ArrayList<>());
        SecurityGuard guard = new SecurityGuard(1L, "Bob", wp, 5000L);

        //For id "1" return guard, else throw EntityNotFoundException
        Mockito.when(guardService.readByID(1L)).thenReturn(guard);
        Mockito.when(guardService.readByID(not(eq(1L)))).thenThrow(EntityNotFoundException.class);
        //HTTP request return json representation for guard
        mockMvc.perform(get("/guards/1")) //HTTP request
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.idGuard", Matchers.is(1)))
                .andExpect(jsonPath("$.name", Matchers.is("Bob")))
                .andExpect(jsonPath("$.idWorkingPosition", Matchers.is("Normal")))
                .andExpect(jsonPath("$.salary", Matchers.is(5000)))
                .andExpect(jsonPath("$.idObserved").isEmpty());

        // If guard does not exist, then return HTTP status Not found
        mockMvc.perform(get("/guards/404"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testCreateExisting() throws Exception {
        Mockito.when(wpService.readByID("security"))
                .thenReturn(new WorkingPosition("security", 1000L, new ArrayList<>()));
        doThrow(new EntityStateException()).when(guardService).create(any(SecurityGuard.class));

        //Try to create an existing guard return HTTP status conflict
        mockMvc.perform(post("/guards")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"Guard\",\"idWorkingPosition\":\"security\",\"salary\":1000}"))
                .andExpect(status().isConflict());
    }

    @Test
    public void testCreate() throws Exception {
        WorkingPosition wp = new WorkingPosition("Normal", 1000L, new ArrayList<>());
        SecurityGuard guard = new SecurityGuard(1L, "Bob", wp, 5000L);
        //Using in guardService.create(guard)
        Mockito.when(wpService.readByID("Normal")).thenReturn(wp);
        Mockito.when(guardService.create(guard)).thenReturn(guard);
        //Return HTTP status ok after created the guard and json representation
        mockMvc.perform(post("/guards")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"idGuard\": 1,\"name\":\"Bob\",\"idWorkingPosition\":\"Normal\",\"salary\":5000}"))
                .andExpect(status().isOk()) // ожидаем, что статс ок
                .andExpect(jsonPath("$.idGuard", Matchers.is(1)))
                .andExpect(jsonPath("$.name", Matchers.is("Bob")))
                .andExpect(jsonPath("$.idWorkingPosition", Matchers.is("Normal")))
                .andExpect(jsonPath("$.salary", Matchers.is(5000)))
                .andExpect(jsonPath("$.idObserved").isEmpty());


        //Check with class ArgumentCaptor, that method create was call 1x with correctly data
        ArgumentCaptor<SecurityGuard> argumentCaptor = ArgumentCaptor.forClass(SecurityGuard.class);
        Mockito.verify(guardService, Mockito.times(1)).create(argumentCaptor.capture());
        var guardProvidedToService = argumentCaptor.getValue();
        assertEquals(1, guardProvidedToService.getIdGuard());
        assertEquals("Bob", guardProvidedToService.getName());
        assertEquals(wp, guardProvidedToService.getWorkingPosition());
        assertEquals(5000L, guardProvidedToService.getSalary());
        assertEquals(new ArrayList<>() ,guardProvidedToService.getObserved());
    }

    @Test
    void testDelete() throws Exception{

        Mockito.when(guardService.readByID(not(eq(1L)))).thenThrow(EntityNotFoundException.class);

        // If guard does not exist, then return HTTP status Not found
        mockMvc.perform(get("/guards/404"))
                .andExpect(status().isNotFound());
        //... deleteById never called
        verify(guardService, never()).deleteById(any());

        //Delete return HTTP status Ok
        mockMvc.perform(delete("/guards/1"))
                .andExpect(status().isOk());
        //...deleteById was called
        verify(guardService, times(1)).deleteById(1L);
    }

    @Test
    void testUpdate() throws Exception{
        WorkingPosition wp = new WorkingPosition("Normal", 1000L, new ArrayList<>());
        SecurityGuard guard = new SecurityGuard(1L, "Bob", wp, 5000L);
        //  Create a guard that will be returned after the update
        SecurityGuard guardUpd = new SecurityGuard(1L, "Bobster", wp, 5000L);
        //Using in guardService.update(id, guard)
        Mockito.when(wpService.readByID("Normal")).thenReturn(wp);
        Mockito.when(guardService.update(1L, guard)).thenReturn(guardUpd);

        //Return HTTP status ok after update the guard and json representation
        mockMvc.perform(put("/guards/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"idGuard\": 1,\"name\":\"Bobster\",\"idWorkingPosition\":\"Normal\",\"salary\":5000}"))
                .andExpect(status().isOk()) // ожидаем, что статс ок
                .andExpect(jsonPath("$.idGuard", Matchers.is(1)))
                .andExpect(jsonPath("$.name", Matchers.is("Bobster")))
                .andExpect(jsonPath("$.idWorkingPosition", Matchers.is("Normal")))
                .andExpect(jsonPath("$.salary", Matchers.is(5000)))
                .andExpect(jsonPath("$.idObserved").isEmpty());

        //Check with class ArgumentCaptor, that method create was call 1x with correctly data
        ArgumentCaptor<SecurityGuard> argumentCaptor = ArgumentCaptor.forClass(SecurityGuard.class);
        Mockito.verify(guardService, Mockito.times(1)).update( eq(1L), argumentCaptor.capture());
        var guardProvidedToService = argumentCaptor.getValue();
        assertEquals(1, guardProvidedToService.getIdGuard());
        assertEquals("Bobster", guardProvidedToService.getName());
        assertEquals(wp, guardProvidedToService.getWorkingPosition());
        assertEquals(5000L, guardProvidedToService.getSalary());
        assertEquals(new ArrayList<>() ,guardProvidedToService.getObserved());
    }

    @Test
    public void testUpdateNotExisting() throws Exception {
        Mockito.when(wpService.readByID("Normal"))
                .thenReturn(new WorkingPosition("Normal", 1000L, new ArrayList<>()));
        doThrow(new EntityNotFoundException()).when(guardService).update(any(Long.class), any(SecurityGuard.class));

        //Try to update a not existing guard return HTTP status Not found
        mockMvc.perform(put("/guards/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"idGuard\": 1,\"name\":\"Bob\",\"idWorkingPosition\":\"Normal\",\"salary\":5000}"))
                .andExpect(status().isNotFound());
    }

    @Test
    void testReadAllObserved() throws Exception{
        WorkingPosition wp = new WorkingPosition("Normal", 1000L, new ArrayList<>());
        SecurityGuard guard = new SecurityGuard(1L, "Bob", wp, 5000L);
        Cell cell = new Cell(1L, 10d, 10d);
        Prisoner p1 = new Prisoner(1L, "Antonio", "Normal", cell);
        Prisoner p2 = new Prisoner(2L, "Wolfeschlegelsteinhausenbergerdorff", "Normal", cell);
        p1.setWatched(List.of(guard));
        p2.setWatched(List.of(guard));
        List<Prisoner> prisoners = List.of(p1, p2);
        guard.setObserved(prisoners);
        Mockito.when(wpService.readByID("Normal")).thenReturn(wp);
        Mockito.when(guardService.readByID(1L)).thenReturn(guard);
        Mockito.when(guardService.readByID(not(eq(1L)))).thenThrow(EntityNotFoundException.class);
        Mockito.when(guardService.readObserved(1L)).thenReturn(prisoners);
        Mockito.when(guardService.readObserved(not(eq(1L)))).thenThrow(EntityNotFoundException.class);

        mockMvc.perform(get("/guards/404/prisoners"))
                .andExpect(status().isNotFound());

        mockMvc.perform(get("/guards/1/prisoners"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)))
                .andExpect(jsonPath("$[0].idPrisoner", Matchers.is(1)))
                .andExpect(jsonPath("$[0].name", Matchers.is("Antonio")))
                .andExpect(jsonPath("$[0].regime", Matchers.is("Normal")))
                .andExpect(jsonPath("$[0].idCell", Matchers.is(1)))
                .andExpect(jsonPath("$[0].idWatched").isNotEmpty())
                .andExpect(jsonPath("$[1].idPrisoner", Matchers.is(2)))
                .andExpect(jsonPath("$[1].name", Matchers.is("Wolfeschlegelsteinhausenbergerdorff")))
                .andExpect(jsonPath("$[1].regime", Matchers.is("Normal")))
                .andExpect(jsonPath("$[1].idCell", Matchers.is(1)))
                .andExpect(jsonPath("$[1].idWatched").isNotEmpty());
    }

    @Test
    void testAddObserved() throws Exception{
        WorkingPosition wp = new WorkingPosition("Normal", 1000L, new ArrayList<>());
        Cell cell = new Cell(1L, 10d, 10d);
        SecurityGuard guard = new SecurityGuard(1L, "Bob", wp, 5000L);
        SecurityGuard guardAfter = new SecurityGuard(1L, "Bob", wp, 5000L);
        Prisoner p1 = new Prisoner(1L, "Antonio", "Normal", cell);
        guardAfter.setObserved(List.of(p1));
        Mockito.when(wpService.readByID("Normal")).thenReturn(wp);
        Mockito.when(guardService.readByID(1L)).thenReturn(guard);
        Mockito.when(guardService.readByID(not(eq(1L)))).thenThrow(EntityNotFoundException.class);
        Mockito.when(guardService.addObserve(1L, 1L)).thenReturn(guardAfter);

        mockMvc.perform(post("/guards/1/prisoners/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.idGuard", Matchers.is(1)))
                .andExpect(jsonPath("$.name", Matchers.is("Bob")))
                .andExpect(jsonPath("$.idWorkingPosition", Matchers.is("Normal")))
                .andExpect(jsonPath("$.salary", Matchers.is(5000)))
                .andExpect(jsonPath("$.idObserved").value(1));
    }

    @Test
    void testDeleteObserved() throws Exception{
        WorkingPosition wp = new WorkingPosition("Normal", 1000L, new ArrayList<>());
        SecurityGuard guard = new SecurityGuard(1L, "Bob", wp, 5000L);
        SecurityGuard guardAfter = new SecurityGuard(1L, "Bob", wp, 5000L);
        guard.setObserved(List.of(new Prisoner()));

        Mockito.when(guardService.readByID(not(eq(1L)))).thenThrow(EntityNotFoundException.class);
        Mockito.when(guardService.readByID(1L)).thenReturn(guard);
        Mockito.when(guardService.deleteObserve(1L, 1L)).thenReturn(guardAfter);

        //Delete return HTTP status Ok
        mockMvc.perform(delete("/guards/1/prisoners/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.idGuard", Matchers.is(1)))
                .andExpect(jsonPath("$.name", Matchers.is("Bob")))
                .andExpect(jsonPath("$.idWorkingPosition", Matchers.is("Normal")))
                .andExpect(jsonPath("$.salary", Matchers.is(5000)))
                .andExpect(jsonPath("$.idObserved").isEmpty());
        //...deleteById was called
        verify(guardService, times(1)).deleteObserve(1L, 1L);
    }

    @Test
    void testAddPositionToGuard() throws Exception{
        WorkingPosition wp = new WorkingPosition("Normal", 1000L, new ArrayList<>());
        WorkingPosition wp2 = new WorkingPosition("Sec", 10000L, new ArrayList<>());
        SecurityGuard guard = new SecurityGuard(1L, "Bob", wp, 5000L);
        SecurityGuard guard2 = new SecurityGuard(1L, "Bob", wp2, 10000L);

        Mockito.when(wpService.readByID("Normal")).thenReturn(wp);
        Mockito.when(wpService.readByID("Sec")).thenReturn(wp2);
        Mockito.when(guardService.readByID(1L)).thenReturn(guard);
        Mockito.when(guardService.readByID(not(eq(1L)))).thenThrow(EntityNotFoundException.class);
        Mockito.when(guardService.changeWorkPos(1L, "Sec")).thenReturn(guard2);

        mockMvc.perform(post("/guards/1/positions/Sec"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.idGuard", Matchers.is(1)))
                .andExpect(jsonPath("$.name", Matchers.is("Bob")))
                .andExpect(jsonPath("$.idWorkingPosition", Matchers.is("Sec")))
                .andExpect(jsonPath("$.salary", Matchers.is(10000)))
                .andExpect(jsonPath("$.idObserved").isEmpty());
    }
}