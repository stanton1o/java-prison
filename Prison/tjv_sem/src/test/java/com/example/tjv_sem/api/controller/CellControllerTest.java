package com.example.tjv_sem.api.controller;


import com.example.tjv_sem.api.controller.dto.converter.CellConverter;
import com.example.tjv_sem.api.controller.dto.converter.PrisonerConverter;
import com.example.tjv_sem.api.exception.EntityNotFoundException;
import com.example.tjv_sem.api.exception.EntityStateException;
import com.example.tjv_sem.business.cell.CellService;
import com.example.tjv_sem.domain.Cell;
import com.example.tjv_sem.domain.Prisoner;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import java.util.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CellController.class)
class CellControllerTest {

    @MockBean
    CellService cellService;
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    MockMvc mockMvc;

    @Autowired
    CellConverter cellConverter;
    @Autowired
    PrisonerConverter prisonerConverter;

    @TestConfiguration
    static class TestConfig {

        @Bean
        public CellConverter cellConverter() {
            return new CellConverter();
        }

        @Bean
        public PrisonerConverter prisonerConverter(CellService cellService){
            return new PrisonerConverter(cellService);
        }

    }

    @Test
    void testGetById() throws Exception {
        Cell cell = new Cell(1L, 10d, 10d);

        Mockito.when(cellService.readByID(1L)).thenReturn(cell);
        Mockito.when(cellService.readByID(not(eq(1L)))).thenThrow(EntityNotFoundException.class);
        mockMvc.perform(get("/cells/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.idCell", Matchers.is(1)))
                .andExpect(jsonPath("$.size", Matchers.is(10d)))
                .andExpect(jsonPath("$.comfort", Matchers.is(10d)))
                .andExpect(jsonPath("$.idPrisoners").isEmpty());

        mockMvc.perform(get("/cells/2"))
                .andExpect(status().isNotFound());

        mockMvc.perform(get("/cells/404"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testCreateExisting() throws Exception {
        doThrow(new EntityStateException()).when(cellService).create(any(Cell.class));

        mockMvc.perform(post("/cells")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"idCell\": 1," +
                                "    \"size\": 10.0," +
                                "    \"comfort\": 10.0" +
                                "}"))
                .andExpect(status().isConflict());
    }

    @Test
    public void testCreate() throws Exception {
        Cell cell = new Cell(1L, 10d, 10d);

        Mockito.when(cellService.create(cell)).thenReturn(cell);
        mockMvc.perform(post("/cells")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"idCell\": 1," +
                                "    \"size\": 10.0," +
                                "    \"comfort\": 10.0" +
                                "}"))
                .andExpect(status().isOk()) // ожидаем, что статс ок
                .andExpect(jsonPath("$.idCell", Matchers.is(1)))
                .andExpect(jsonPath("$.size", Matchers.is(10d)))
                .andExpect(jsonPath("$.comfort", Matchers.is(10d)))
                .andExpect(jsonPath("$.idPrisoners").isEmpty());

        ArgumentCaptor<Cell> argumentCaptor = ArgumentCaptor.forClass(Cell.class);
        Mockito.verify(cellService, Mockito.times(1)).create(argumentCaptor.capture());
        Cell cellProvidedToService = argumentCaptor.getValue();
        assertEquals(1, cellProvidedToService.getIdCell());
        assertEquals(10d, cellProvidedToService.getSize());
        assertEquals(10d, cellProvidedToService.getComfort());
        assertEquals(new ArrayList<>() ,cellProvidedToService.getPrisoners());
    }

    @Test
    void testDelete() throws Exception{
        Cell cell = new Cell(1L, 10d, 10d);

        Mockito.when(cellService.readByID(1L)).thenReturn(cell);
        Mockito.when(cellService.readByID(not(eq(1L)))).thenThrow(EntityNotFoundException.class);

        mockMvc.perform(get("/cells/404"))
                .andExpect(status().isNotFound());
        verify(cellService, never()).deleteById(any());

        mockMvc.perform(delete("/cells/1"))
                .andExpect(status().isOk());
        verify(cellService, times(1)).deleteById(1L);
    }

    @Test
    void testUpdate() throws Exception{
        Cell cell = new Cell(1L, 10d, 10d);
        Cell cellToUpd = new Cell(1L, 11d, 11d);

        Mockito.when(cellService.readByID(1L)).thenReturn(cell);
        Mockito.when(cellService.readByID(not(eq(1L)))).thenThrow(EntityNotFoundException.class);
        Mockito.when(cellService.update(1L, cell)).thenReturn(cellToUpd);
        mockMvc.perform(put("/cells/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"idCell\": 1," +
                                "    \"size\": 11.0," +
                                "    \"comfort\": 11.0" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.idCell", Matchers.is(1)))
                .andExpect(jsonPath("$.size", Matchers.is(11d)))
                .andExpect(jsonPath("$.comfort", Matchers.is(11d)))
                .andExpect(jsonPath("$.idPrisoners").isEmpty());


        ArgumentCaptor<Cell> argumentCaptor = ArgumentCaptor.forClass(Cell.class);
        Mockito.verify(cellService, Mockito.times(1)).update( eq(1L), argumentCaptor.capture());
        Cell cellProvidedToService = argumentCaptor.getValue();
        assertEquals(1, cellProvidedToService.getIdCell());
        assertEquals(11d, cellProvidedToService.getComfort());
        assertEquals(11d, cellProvidedToService.getSize());
        assertEquals(new ArrayList<>() ,cellProvidedToService.getPrisoners());
    }

    @Test
    public void testUpdateNotExisting() throws Exception {
        doThrow(new EntityNotFoundException()).when(cellService).update(any(Long.class), any(Cell.class));

        mockMvc.perform(put("/cells/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"idCell\": 1," +
                                "    \"size\": 11.0," +
                                "    \"comfort\": 11.0" +
                                "}"))
                .andExpect(status().isNotFound());
    }

    @Test
    void readAllPrisoners() throws Exception{

        Cell cell = new Cell(1L, 10d, 10d);
        Prisoner p1 = new Prisoner(1L, "Antonio", "Normal", cell);
        Prisoner p2 = new Prisoner(2L, "Wolfeschlegelsteinhausenbergerdorff", "Normal", cell);
        List<Prisoner> prisoners = List.of(p1, p2);
        cell.setPrisoners(prisoners);
        Mockito.when(cellService.readByID(1L)).thenReturn(cell);
        Mockito.when(cellService.readByID(not(eq(1L)))).thenThrow(EntityNotFoundException.class);
        Mockito.when(cellService.readPrisonersInCellById(1L)).thenReturn(prisoners);
        mockMvc.perform(get("/cells/1/prisoners"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)))
                .andExpect(jsonPath("$[0].idPrisoner", Matchers.is(1)))
                .andExpect(jsonPath("$[0].name", Matchers.is("Antonio")))
                .andExpect(jsonPath("$[0].regime", Matchers.is("Normal")))
                .andExpect(jsonPath("$[0].idCell", Matchers.is(1)))
                .andExpect(jsonPath("$[1].idPrisoner", Matchers.is(2)))
                .andExpect(jsonPath("$[1].name", Matchers.is("Wolfeschlegelsteinhausenbergerdorff")))
                .andExpect(jsonPath("$[1].regime", Matchers.is("Normal")))
                .andExpect(jsonPath("$[1].idCell", Matchers.is(1)));
    }

    @Test
    void findCellsBySalary() throws Exception{
        Cell cell = new Cell(1L, 10d, 10d);
        Cell cell2 = new Cell(2L, 11d, 10d);
        List<Cell> cells = List.of(cell, cell2);
        Mockito.when(cellService.findCellsBySalary(5000L)).thenReturn(cells);
        mockMvc.perform(get("/cells/find/5000"))
                .andExpect(jsonPath("$", Matchers.hasSize(2)))
                .andExpect(jsonPath("$[0].idCell", Matchers.is(1)))
                .andExpect(jsonPath("$[0].comfort", Matchers.is(10d)))
                .andExpect(jsonPath("$[0].size", Matchers.is(10d)))
                .andExpect(jsonPath("$[0].idPrisoners").isEmpty())
                .andExpect(jsonPath("$[1].idCell", Matchers.is(2)))
                .andExpect(jsonPath("$[1].comfort", Matchers.is(10d)))
                .andExpect(jsonPath("$[1].size", Matchers.is(11d)))
                .andExpect(jsonPath("$[1].idPrisoners").isEmpty());
    }
}
