package com.example.prisonclient.data;

import com.example.prisonclient.model.GuardDto;
import com.example.prisonclient.model.PrisonerDto;
import com.example.prisonclient.ui.guard.GuardView;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;
import java.util.List;

@Component
public class GuardClient {
    private final WebClient webClient;
    public final GuardView guardView;

    public GuardClient(@Value("${server_url}") String url, GuardView guardView) {
        webClient = WebClient.create(url + "/guards");
        this.guardView = guardView;
    }

    public GuardDto create(GuardDto guardDto){
        return webClient.post()
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(guardDto)
                .retrieve()
                .bodyToMono(GuardDto.class)
                .block(Duration.ofSeconds(5));
    }

    public GuardDto getById(Long id) {
        return webClient.get().uri("/" + id)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(GuardDto.class)
                .block();
    }

    public GuardDto update(Long id, GuardDto guardDto) {
        return webClient.put().uri("/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(guardDto)
                .retrieve()
                .bodyToMono(GuardDto.class)
                .block(Duration.ofSeconds(5));
    }

    public void delete(Long id) {
        webClient.delete().uri("/" + id)
                .retrieve()
                .toBodilessEntity()
                .block();
    }

    public List<PrisonerDto> readAllObserved(Long id){
        return webClient.get().uri("/" + id + "/prisoners")
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToFlux(PrisonerDto.class)
                .collectList().block();
    }

    public GuardDto addObserved( Long idGuard,  Long idPrisoner){
        return webClient.post().uri("/" + idGuard + "/prisoners/" + idPrisoner)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(GuardDto.class)
                .block(Duration.ofSeconds(5));
    }

    public GuardDto deleteObserved( Long idGuard,  Long idPrisoner){
        return webClient.delete().uri("/" + idGuard + "/prisoners/" + idPrisoner)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(GuardDto.class)
                .block();
    }

    public GuardDto addPositionToGuard( Long idGuard,  String idWorkPos){
        return webClient.post().uri("/" + idGuard + "/positions/" + idWorkPos)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(GuardDto.class)
                .block(Duration.ofSeconds(5));
    }
}
