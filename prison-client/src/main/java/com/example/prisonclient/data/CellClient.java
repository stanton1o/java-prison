package com.example.prisonclient.data;

import com.example.prisonclient.model.CellDto;
import com.example.prisonclient.model.PrisonerDto;
import com.example.prisonclient.ui.cell.CellView;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;
import java.util.List;

@Component
public class CellClient {

    private final WebClient webClient;
    public final CellView cellView;

    public CellClient(@Value("${server_url}") String url, CellView cellView) {
        webClient = WebClient.create(url + "/cells");
        this.cellView = cellView;
    }

    public CellDto create(CellDto cell){
        return webClient.post()
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(cell)
                .retrieve()
                .bodyToMono(CellDto.class)
                .block(Duration.ofSeconds(5));
    }

    public CellDto getById(Long id) {
        return webClient.get().uri("/" + id)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(CellDto.class)
                .block();
    }

    public CellDto update(Long id, CellDto cell) {
        return webClient.put().uri("/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(cell)
                .retrieve()
                .bodyToMono(CellDto.class)
                .block(Duration.ofSeconds(5));
    }

    public void delete(Long id) {
        webClient.delete().uri("/" + id)
                .retrieve()
                .toBodilessEntity()
                .block();
    }

    public List<PrisonerDto> readAllPrisoners(Long id){
        return webClient.get().uri("/" + id + "/prisoners")
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToFlux(PrisonerDto.class)
                .collectList().block();
    }

    public List<CellDto> findCellsBySalary(Long id){
        return webClient.get().uri("/find/" + id)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToFlux(CellDto.class)
                .collectList().block();
    }

}
