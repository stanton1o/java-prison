package com.example.prisonclient.data;

import com.example.prisonclient.model.GuardDto;
import com.example.prisonclient.model.PrisonerDto;
import com.example.prisonclient.ui.prisoner.PrisonerView;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;
import java.util.List;

@Component
public class PrisonerClient {
    private final WebClient webClient;
    public final PrisonerView prisonerView;

    public PrisonerClient(@Value("${server_url}") String url, PrisonerView prisonerView) {
        webClient = WebClient.create(url + "/prisoners");
        this.prisonerView = prisonerView;
    }

    public PrisonerDto create(PrisonerDto prisoner){
        return webClient.post()
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(prisoner)
                .retrieve()
                .bodyToMono(PrisonerDto.class)
                .block(Duration.ofSeconds(5));
    }

    public PrisonerDto getById(Long id) {
        return webClient.get().uri("/" + id)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(PrisonerDto.class)
                .block();
    }

    public PrisonerDto update(Long id, PrisonerDto prisoner) {
        return webClient.put().uri("/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(prisoner)
                .retrieve()
                .bodyToMono(PrisonerDto.class)
                .block(Duration.ofSeconds(5));
    }

    public void delete(Long id) {
        webClient.delete().uri("/" + id)
                .retrieve()
                .toBodilessEntity()
                .block();
    }

    public List<GuardDto> readAllWatched(Long id){
        return webClient.get().uri("/" + id + "/guards")
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToFlux(GuardDto.class)
                .collectList().block();
    }

    public PrisonerDto addPrisonerToCell(Long idPrisoner, Long idCell){
        return webClient.post().uri("/" + idPrisoner + "/cells/" + idCell)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(PrisonerDto.class)
                .block(Duration.ofSeconds(5));
    }
}
