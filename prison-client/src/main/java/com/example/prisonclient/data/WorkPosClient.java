package com.example.prisonclient.data;

import com.example.prisonclient.model.GuardDto;
import com.example.prisonclient.model.WorkPosDto;
import com.example.prisonclient.ui.position.PositionView;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.Duration;
import java.util.List;

@Component
public class WorkPosClient {
    private final WebClient webClient;
    public final PositionView positionView;

    public WorkPosClient(@Value("${server_url}") String url, PositionView positionView) {
        webClient = WebClient.create(url + "/positions");
        this.positionView = positionView;
    }

    public WorkPosDto create(WorkPosDto pos){
        return webClient.post()
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(pos)
                .retrieve()
                .bodyToMono(WorkPosDto.class)
                .block(Duration.ofSeconds(5));
    }

    public WorkPosDto getById(String id) {
        return webClient.get().uri("/" + id)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(WorkPosDto.class)
                .block();
    }

    public WorkPosDto update(String id, WorkPosDto pos) {
        return webClient.put().uri("/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(pos)
                .retrieve()
                .bodyToMono(WorkPosDto.class)
                .block(Duration.ofSeconds(5));
    }

    public void delete(String id) {
        webClient.delete().uri("/" + id)
                .retrieve()
                .toBodilessEntity()
                .block();
    }

    public List<GuardDto> readAllGuards(String id){
        return webClient.get().uri("/" + id + "/guards")
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToFlux(GuardDto.class)
                .collectList().block();
    }
}
