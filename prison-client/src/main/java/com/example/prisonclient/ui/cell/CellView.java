package com.example.prisonclient.ui.cell;

import com.example.prisonclient.model.CellDto;
import com.example.prisonclient.ui.View;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClientException;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.reactive.function.client.WebClientResponseException;

@Component
public class CellView extends View {

    public void print(CellDto cell) {
        final String[] reservations = {""};
        System.out.println("Id: " + cell.getIdCell());
        System.out.println("Size: " + cell.getSize());
        System.out.println("Comfort: " + cell.getComfort());
        System.out.print("Prisoners: [");
            cell.getIdPrisoners().forEach(System.out::println);
        System.out.println(" ]");
        System.out.println("-----------------------------");
    }
}
