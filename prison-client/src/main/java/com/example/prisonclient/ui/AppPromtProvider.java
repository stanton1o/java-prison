package com.example.prisonclient.ui;

import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStyle;
import org.springframework.shell.jline.PromptProvider;
import org.springframework.stereotype.Component;

@Component
public class AppPromtProvider implements PromptProvider {

    @Override
    public AttributedString getPrompt() {
        return new AttributedString("Prison >: ", AttributedStyle.DEFAULT.foreground(AttributedStyle.BLUE));
    }
}
