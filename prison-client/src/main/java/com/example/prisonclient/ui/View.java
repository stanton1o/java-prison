package com.example.prisonclient.ui;

import org.springframework.web.reactive.function.client.WebClientException;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.reactive.function.client.WebClientResponseException;

public abstract class View {

    public void printErrorGeneric(Throwable e) {
        if (e instanceof WebClientRequestException) {
            System.err.println("Cannot connect to API :(");
        }
    }
    public void printError(WebClientException e, String entity, String op) {
        if (e instanceof WebClientResponseException.Conflict)
            System.err.println( entity + " can't be " + op + "ed");
        else if (e instanceof WebClientResponseException.NotFound)
            System.err.println( entity + " doesn't exist");
        else if (e instanceof WebClientResponseException.BadRequest)
            System.err.println("Bad request, try again");
        else
            printErrorGeneric(e);
    }

}
