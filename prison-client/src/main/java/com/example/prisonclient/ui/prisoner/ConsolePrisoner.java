package com.example.prisonclient.ui.prisoner;

import com.example.prisonclient.data.PrisonerClient;
import com.example.prisonclient.model.PrisonerDto;
import com.example.prisonclient.ui.guard.GuardView;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.web.reactive.function.client.WebClientException;

@ShellComponent
public class ConsolePrisoner {

    private final PrisonerClient prisonerClient;
    private final PrisonerView prisonerView;
    private final GuardView guardView;

    public ConsolePrisoner(PrisonerClient prisonerClient, PrisonerView prisonerView, GuardView guardView) {
        this.prisonerClient = prisonerClient;
        this.prisonerView = prisonerView;
        this.guardView = guardView;
    }

    @ShellMethod("Create a new prisoner: name, regime, idCell")
    public void createPrisoner(String name, String regime, Long idCell){
        try{
            var prisoner = new PrisonerDto(name, regime, idCell);
            prisonerView.print(prisonerClient.create(prisoner));
        } catch (WebClientException e){
            prisonerView.printError(e, "prisoner", "create");
        }
    }

    @ShellMethod("Delete prisoner by id")
    public void deletePrisoner(Long id) {
        try {
            prisonerClient.delete(id);
        } catch (WebClientException e) {
            prisonerView.printError(e, "prisoner", "delete");
        }
    }

    @ShellMethod("Get prisoner by id")
    public void getPrisonerById(Long id) {
        try {
            prisonerView.print(prisonerClient.getById(id));
        } catch (WebClientException e) {
            prisonerView.printError(e, "prisoner", "find");
        }
    }

    @ShellMethod("Update prisoner: id, name, regime")
    public void updatePrisoner(Long id, String name, String regime){
        try{
            var prisoner = new PrisonerDto(name, regime,prisonerClient.getById(id).getIdCell());
            prisonerView.print(prisonerClient.update(id, prisoner));
        } catch (WebClientException e){
            prisonerView.printError(e, "prisoner", "update");
        }
    }

    @ShellMethod("Get guards watched on prisoner by prisoner id")
    public void readAllWatchers(Long id) {
        try {
            prisonerClient.readAllWatched(id).forEach(guardView::print);
        } catch (WebClientException e) {
            prisonerView.printError(e, "prisoner", "watchers read");
        }
    }

    @ShellMethod("Add prisoner to cell: idPrisoner, idCell")
    public void addPrisonerToCell(Long idPrisoner, Long idCell) {
        try {
            prisonerView .print(prisonerClient.addPrisonerToCell(idPrisoner, idCell));
        } catch (WebClientException e) {
            prisonerView.printError(e, "prisoner or cell", "add");
        }
    }
}
