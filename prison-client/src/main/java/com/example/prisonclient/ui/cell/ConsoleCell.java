package com.example.prisonclient.ui.cell;

import com.example.prisonclient.data.CellClient;
import com.example.prisonclient.model.CellDto;
import com.example.prisonclient.ui.prisoner.PrisonerView;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.web.reactive.function.client.WebClientException;

import java.util.ArrayList;

@ShellComponent
public class ConsoleCell {

    private final CellClient cellClient;
    private final CellView cellView;
    private final PrisonerView prisonerView;

    public ConsoleCell(CellClient cellClient, CellView cellView, PrisonerView prisonerView) {
        this.cellClient = cellClient;
        this.cellView = cellView;
        this.prisonerView = prisonerView;
    }

    @ShellMethod("Create a new cell: Double size, Double comfort")
    public void createCell(Double size, Double comfort){
        try{
            var cell = new CellDto(null, size, comfort, new ArrayList<>());
            var res = cellClient.create(cell);
            cellView.print(res);
        } catch (WebClientException e){
            cellView.printError(e, "cell", "create");
        }
    }

    @ShellMethod("Delete cell by id")
    public void deleteCell(Long id) {
        try {
            cellClient.delete(id);
        } catch (WebClientException e) {
            cellView.printError(e, "cell", "delete");
        }
    }

    @ShellMethod("Get cell by id")
    public void getCellById(Long id) {
        try {
            cellView.print(cellClient.getById(id));
        } catch (WebClientException e) {
            cellView.printError(e, "cell", "find");
        }
    }

    @ShellMethod("Update cell: Long id , Double size, Double comfort")
    public void updateCell(Long id , Double size, Double comfort) {
        try {
             cellView.print(cellClient.update(id, new CellDto(size, comfort)));
        }catch (WebClientException e){
            cellView.printError(e, "cell", "update");
        }
    }

    @ShellMethod("Get prisoners in cell by cell id")
    public void readAllPrisoners(Long id) {
        try {
            cellClient.readAllPrisoners(id).forEach(prisonerView::print);
        } catch (WebClientException e) {
            cellView.printError(e, "cell", "prisoners read");
        }
    }

    @ShellMethod("Get cells by salary")
    public void findBySalary(Long salary) {
        try {
            cellClient.findCellsBySalary(salary).forEach(cellView::print);
        } catch (WebClientException e) {
            cellView.printError(e, "cell", "prisoners read");
        }
    }
}
