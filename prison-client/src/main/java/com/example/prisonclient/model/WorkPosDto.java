package com.example.prisonclient.model;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class WorkPosDto {
    private String positionName;
    private Long minSalary;
    private List<Long> idSecurityGuards;

    public WorkPosDto(String positionName, Long minSalary) {
        this.positionName = positionName;
        this.minSalary = minSalary;
    }

    public WorkPosDto() {
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public Long getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(Long minSalary) {
        this.minSalary = minSalary;
    }

    public List<Long> getIdSecurityGuards() {
        return idSecurityGuards;
    }

    public void setIdSecurityGuards(List<Long> idSecurityGuards) {
        this.idSecurityGuards = idSecurityGuards;
    }
}
