package com.example.prisonclient.model;
import java.util.ArrayList;
import java.util.List;

public class GuardDto {
    private Long idGuard;
    private String name;
    private String idWorkingPosition;
    private Long salary;
    private List<Long> idObserved = new ArrayList<>();

    public GuardDto(String name, String idWorkingPosition, Long salary) {
        this.name = name;
        this.idWorkingPosition = idWorkingPosition;
        this.salary = salary;
    }

    public GuardDto() {
    }

    public Long getIdGuard() {
        return idGuard;
    }

    public void setIdGuard(Long idGuard) {
        this.idGuard = idGuard;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdWorkingPosition() {
        return idWorkingPosition;
    }

    public void setIdWorkingPosition(String idWorkingPosition) {
        this.idWorkingPosition = idWorkingPosition;
    }

    public Long getSalary() {
        return salary;
    }

    public void setSalary(Long salary) {
        this.salary = salary;
    }

    public List<Long> getIdObserved() {
        return idObserved;
    }

    public void setIdObserved(List<Long> idObserved) {
        this.idObserved = idObserved;
    }
}
