package com.example.prisonclient.model;

import java.util.ArrayList;
import java.util.List;

public class CellDto {
    private Long idCell;
    private Double size;
    private Double comfort;
    private List<Long> idPrisoners = new ArrayList<>();

    public CellDto(Long idCell, Double size, Double comfort, List<Long> idPrisoners) {
        this.idCell = idCell;
        this.size = size;
        this.comfort = comfort;
        this.idPrisoners = idPrisoners;
    }

    public CellDto(Double size, Double comfort) {
        this.size = size;
        this.comfort = comfort;
    }

    public CellDto() {
    }

    public Long getIdCell() {
        return idCell;
    }

    public void setIdCell(Long idCell) {
        this.idCell = idCell;
    }

    public Double getSize() {
        return size;
    }

    public void setSize(Double size) {
        this.size = size;
    }

    public Double getComfort() {
        return comfort;
    }

    public void setComfort(Double comfort) {
        this.comfort = comfort;
    }

    public List<Long> getIdPrisoners() {
        return idPrisoners;
    }

    public void setIdPrisoners(List<Long> idPrisoners) {
        this.idPrisoners = idPrisoners;
    }
}
