About project:
=========================
A simple program that allows you to manage a prison. The main goal is to learn how to work with Java, Spring, REST API.

Prison API tutorial
===========================

DataBase connect
-----------------------
1. `DataBase url: `jdbc:postgresql://localhost:5432/prison
2. `Login: postgres`
3. `Password: 1243`


How to run this application
---------------------------

1. `run to PrisonAplication to start the server`
2. `Run PrisonClientAplication to start client in console`


API documentation
-----------------

Open `<server_url>/swagger-ui/index.html`, e.g.,
- http://localhost:8080/documentation or
- http://localhost:8080/swagger-ui/index.html

API documentation(not autogen)
------------------------------

1. http://localhost:8080/documentation 
2.`Change  "/v3/api-docs"  to  "/openapi3_0.json"  or  "/openapi3_0.yaml" `